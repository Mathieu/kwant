from matplotlib import pyplot as plt
import numpy as np
from numpy.linalg import solve
from scipy.sparse import csc_matrix

from GF_2D import Greenfunction as GF
#Greenfunction computes the greens function for an infinite square lattice, invariant by translation

E = 1.5

t = 12
t2 = -14
e = 7
e2 = 3.1

#liste de sites a modifier
sites = [((0, 0), (0, 0), e),
         ((0, 0), (1, 1), t),
         ((2, 3), (2, 3), e2),
         ((2, 3), (1, 1), t2)]

def site_dico(sites):
    site = {}
    n = 0
    for start, end, val in sites:
        if start not in site:
            site[start] = n
            n += 1
        if end not in site:
            site[end] = n
            n += 1
    return site


def V_sparse(sites):
    site = site_dico(sites)
    row = np.array([])
    col = np.array([])
    data = np.array([])
    n = len(site)

    for start, end, val in sites:
        i = site[start]
        j = site[end]
        row = np.append(row, i)
        col = np.append(col, j)
        data = np.append(data, val)
        if i != j:
            row = np.append(row, j)
            col = np.append(col, i)
            data = np.append(data, val)

    return csc_matrix((data, (row, col)), shape=(n, n))


def Glueing(sites, alpha_list, beta_list, E):
    V = V_sparse(sites)
    site = site_dico(sites)
    #Etape 1 de la glueing sequence
    n = len(site)
    g_ij = np.zeros(shape=(n, n), dtype=complex)
    G = np.zeros(shape=(n, n), dtype=complex)

    #remplissage de petit g
    for _, (x_i, y_i) in enumerate(site):
        for _, (x_j, y_j) in enumerate(site):
            i = site[(x_i, y_i)]
            j = site[(x_j, y_j)]
            g_ij[i, j] = GF(x_i - x_j, y_i - y_j, E)

    #I use several transpose because the sparse matrix has to be on the left in the product.
    G = solve(np.eye(n) - V.T.dot(g_ij.T).T, g_ij)

    #Etape 2 de la glueing sequence
    Ma = len(alpha_list)
    g_ai = np.empty(shape=(Ma, n), dtype=complex)
    G_ai = np.empty(shape=(Ma, n), dtype=complex)

    #Remplissage de la matrice g_ai
    for a, (x_a, y_a) in enumerate(alpha_list):
        for i, (x_i, y_i) in enumerate(site):
            i = site[(x_i, y_i)]
            g_ai[a, i] = GF(x_a - x_i, y_a - y_i, E)

    G_ai = g_ai + np.dot(g_ai, V.dot(G))

    #Etape 3 de la glueing sequence
    Mb = len(beta_list)
    g_lb = np.zeros(shape=(n, Mb), dtype=complex)
    g_ab = np.zeros(shape=(Ma, Mb), dtype=complex)
    G_ab = np.zeros(shape=(Ma, Mb), dtype=complex)

    for l, (x_l, y_l) in enumerate(site):
        for b, (x_b, y_b) in enumerate(beta_list):
            l = site[(x_l, y_l)]
            g_lb[l, b] = GF(x_l - x_b, y_l - y_b, E)

    for a, (x_a, y_a) in enumerate(alpha_list):
        for b, (x_b, y_b) in enumerate(beta_list):
            g_ab[a, b] = GF(x_a - x_b, y_a - y_b, E)

    G_ab = g_ab + np.dot(G_ai, V.dot(g_lb))
    return G_ab

#Creating dicos, key refers to the modified sites, e.g. {(1, 0), (2, 4)}, and the value to 
#the matrix element V corresponing.
hopping_couples = {}
on_site = {}
for start, end, val in sites:
    if start != end:
        hopping_couples[start, end] = val
        hopping_couples[end, start] = val

    if start == end:
        on_site[start, end] =  val

#Should return 1 if x_i == x_j and y_i == y_j and 0 otherwise.
def test_glueing(x_i, y_i, x_j, y_j, E):
    V = V_sparse(sites)
    def delta(k1, k2):
        return 1 if (k1 == k2) else 0

    #define E - H:
    def H(i1, i2, k1, k2):
        hik = 0
        #on-site terms
        if delta(i1, k1) and delta(i2, k2):
            hik += E - 4

        #Hoppings, check if i and k are nearest neighbors
        if delta(i1, k1):
            if delta(i2, k2+1) or delta(i2, k2-1):
                hik += 1
        if delta(i2, k2):
            if delta(i1, k1+1) or delta(i1, k1-1):
                hik += 1

        #Add the term V to the Hamiltonian 
        if ((i1, i2), (k1, k2)) in on_site:
            hik += -on_site[((i1, i2), (k1, k2))]
        if ((i1, i2), (k1, k2)) in hopping_couples:
            hik += -hopping_couples[((i1, i2), (k1, k2))]
        return hik

    product = 0
    for k1 in range(-100, 100):
        for k2 in range(-100, 100):
            if H(x_i, y_i, k1, k2):
                product += H(x_i, y_i, k1, k2) * Glueing(sites, [(k1, k2)], [(x_j, y_j)], E)
            else: pass
    return product

def main():

    print test_glueing(0, 0, 0, 0, E)


if __name__ == '__main__':
    main()

