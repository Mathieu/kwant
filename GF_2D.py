from __future__ import division
from scipy.integrate import quad
import numpy as np
import cmath as cm
import math
from matplotlib import pyplot
from mpl_toolkits.mplot3d import Axes3D
import matplotlib


def Greenfunction(x, y, E, xprime=0, yprime=0):
    """The main function for this code. It computes the Greenfunction for a square lattice,
    with the distance inter sites a = 1 and the hopping t = 1. 
    The GF is calculate between r = (x, y) and r' = 0 (because of T.I.).
    """
    pi = cm.pi

    if y - yprime != 0:
        pm = np.sign(y)
    else:
        pm = 1

    def E_bar(kx):
        return E / 2 - 2 + math.cos(kx)

    def velocity(kx, ky):
        return - (cm.exp(1j * ky) - cm.exp(-1j * ky)) / (2 * 1j)

    def poles(E, kx):
        e = E_bar(kx)
        if abs(e) <= 1:
            return [pm * math.acos(-e)]
        elif e <= -1:
            return [pm * 1j * np.arccosh(-e)]
        else:
            return [pm * (pi + 1j * np.arccosh(e))]

    def integrand(kx, x, xprime, y, yprime, E):

        somme = 0
        for q in poles(E, kx):
            vel = velocity(kx, q)
            if vel:
                somme += cm.exp(1j * q * (y - yprime)) / vel
        return pm * 1j / 2 * cm.exp(1j * kx * (x - xprime)) * somme

    def re_integrand(*args):
        return integrand(*args).real
    def im_integrand(*args):
        return integrand(*args).imag

    var = (x, xprime, y, yprime, E)
    gf_real = 1 / (2*pi) * quad(re_integrand, -pi, pi, args=var, limit=1000)[0]
    gf_imag = 1 / (2*pi) * quad(im_integrand, -pi, pi, args=var, limit=1000)[0]
    return complex(gf_real, gf_imag)


def Greenfunction_dblquad(x, y, E, epsilon=1e-4):
    """This code returns the same result as the previous one, although slower, less stable.
    It uses a bruteforce integration of the double integral with the small parameter epsilon
    """
    pi = cm.pi

    def integrand(ky, kx, x, y, E):
        Ek = 2 * (2 - math.cos(kx) - math.cos(ky))
        wf = cm.exp(1j * (kx*x + ky*y))
        return wf / (E - Ek + 1j * epsilon)

    def re_integrand(*args):
        return integrand(*args).real
    def im_integrand(*args):
        return integrand(*args).imag

    def re_integral(kx, x, y, E):
        return quad(re_integrand, -pi, pi, args=(kx, x, y, E), limit=1000)[0]
    def im_integral(kx, x, y, E):
        return quad(im_integrand, -pi, pi, args=(kx, x, y, E), limit=1000)[0]

    gf_real = 1 / (2*pi)**2 * quad(re_integral, -pi, pi, args=(x, y, E), limit=1000)[0]
    gf_imag = 1 / (2*pi)**2 * quad(im_integral, -pi, pi, args=(x, y, E), limit=1000)[0]
    return complex(gf_real, gf_imag)


def surf_plot(E, d):

    fig = pyplot.figure()
    ax = fig.gca(projection='3d')
    x = np.linspace(-d, d, 2*d+1)
    y = np.linspace(-d, d, 2*d+1)
    Z1 = np.zeros(shape=(2*d+1, 2*d+1), dtype=complex)
    Z2 = np.zeros(shape=(2*d+1, 2*d+1), dtype=complex)

    for i in range(2*d+1):
        for j in range(2*d+1):
            Z1[i,j] = Greenfunction(x[j], y[i], E)

    x, y = np.meshgrid(x, y)
    surf = ax.plot_surface(x, y, Z1.real, rstride=1, cstride=1, cmap=matplotlib.cm.coolwarm)
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    pyplot.title('Greens function for energy = {}, real part' .format(E))
    pyplot.show()

    fig = pyplot.figure()
    ax = fig.gca(projection='3d')
    surf = ax.plot_surface(x, y, Z1.imag, rstride=1, cstride=1, cmap=matplotlib.cm.coolwarm)
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    pyplot.title('Greens function for energy = {}, imaginary part' .format(E))
    pyplot.show()

    fig.colorbar(surf, aspect=5)


def main():
    surf_plot(2, 20)
    #print test(0, 0, 0.5)


def test(x_j, y_j, E):
    """Code checking if the GF is the inverse of the Hamiltonian, G * H = Id
    """
    #define hamiltonian:
    def H(k1, k2):
        if ((k1 == 0) and (k2 == 0)):
            return 4 
        if ((k1 == 1) and (k2 == 0) or (k1 == -1) and (k2 == 0)):
            return -1
        if ((k1 == 0) and (k2 == 1) or (k1 == 0) and (k2 == -1)):
            return -1
        return 0

    def Gf(k1, k2):
        return Greenfunction(x_j-k1, y_j-k2, E)

    def delta(k1, k2):
        return 1 if ((k1 == 0) and (k2 == 0)) else 0

    D = 6
    product = 0
    for k1 in range(-D//2, D//2):
        for k2 in range(-D//2, D//2):
            if delta(k1, k2) != 0 or H(k1, k2) != 0:
                product += (E * delta(k1, k2) - H(k1, k2)) * Gf(k1, k2)
            else: pass

    return product


def Kramers(x, y, E):
    """Compute the real part of the GF with the imaginary part of the GF(or vice versa)
    , using the Kramers Kronig relation. It is nothing but a check of the code
    """
    sub_div = 100
    def integrand(e, x, y, E):
        return (Greenfunction(x, y, E+e) - 
                Greenfunction(x, y, E-e)) / e if e else 0

    def re_integrand(*args):
        return integrand(*args).real
    def im_integrand(*args):
        return integrand(*args).imag

    real_GF_KK = 1 / (2 * math.pi) * quad(im_integrand, -8, 8, args = (x, y, E),
                                     limit=sub_div)[0]
    Imag_GF_KK = -1 / (2 * math.pi) * quad(re_integrand, -8, 8, args = (x, y, E),
                                      limit = sub_div)[0]

    print "real Kramers:", 'x =', x, 'y =', y, 'Energy =', E, 'GF =', real_GF_KK
    print 'Re GF:', 'x =', x, 'y =', y, 'Energy =', E, 'GF =', Greenfunction(x, y, E).real
    print 'Imag kramers', Imag_GF_KK
    print 'Imag GF', Greenfunction(x, y, E).imag

    return real_GF_KK

if __name__ == '__main__':
    main()
